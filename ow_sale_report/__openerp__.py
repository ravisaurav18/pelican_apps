{
		"name"          : "OW Customized Sales Report",
		"version"       : "1.0",
		"depends"       : ["base","sale","wagon_mail",'crm','report_webkit'],
		"author"        : "RSNT",
		"description"   : """
This module has customized several aspects in Sales & CRM module, including:

- Quotation / Order template
- Leads Management
""",
		"website"       : "",
		"category"      : "Sales",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"data.xml",
							"report/report_sale_order.xml",
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}