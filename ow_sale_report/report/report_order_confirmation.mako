<!DOCTYPE html>
<html>
<head>
	<title>Order Confirmation</title>
	<style>
		.break { page-break-after: always; }
		
		body
		{
			font-size: 12px;
			min-height: 100%;
		}

		table.no_border
		{
		border-collapse:collapse;
		padding:10px;
		width:100%;
		vertical-align: top;
		}
		
		table.grey
		{
		border-collapse:collapse;
		border: 1px solid LightGrey;
		padding:10px;
		}

		td.grey
		{
		border: 1px solid LightGrey;
		}

		table.content
		{
		border-collapse:collapse;
		border: 1px solid black;
		}

		td.content
		{
		border: 1px solid black;
		vertical-align: top;
		padding: 2px;
		}

		td.og_theme
		{
		background-color: #FFD9B9;
		}

		td.total
		{
		border-bottom: 1px solid black;
		vertical-align: top;
		font-weight: bold;
		}

		td.box
		{
		border: 1px solid black;
		padding: 20px;
		}

		th.content
		{
		border-collapse: collapse;
		border-bottom: 1px solid black;
		}

		.hmid
		{
			text-align: center;
		}
		.hright
		{
			text-align: right;
		}
		.vmid
		{
		vertical-align: middle;
		}

		.footer 
		{
			position: absolute;
			right: 0;
			bottom: 0;
			left: 0;
			padding: 1rem;
			background-color: #efefef;
			text-align: center;
		}
		td.data
		{
		border-bottom:thin solid black;
		width:69%;
		}
		
		.var
		{
		font-weight:bold;
		}
		
		.left
		{
			text-align: left;
		}

		.center
		{
			text-align: center;
		}

		.right
		{
			text-align: right;
		}

		.padded
		{
		text-align:center;
		padding-top:30px;
		}
		
		.ttd
		{
		text-align:center;
		padding-bottom:80px;
		width:50%
		}
		
		.nama
		{
		text-align:center;
		width:50%;
		font-weight:bold;
		}
		
	</style>
</head>
<% set status = {
	'draft': 'Draft Quotation',
	'sent': 'Quotation Sent',
	'cancel': 'Cancelled',
	'waiting_date': 'Waiting Schedule',
	'progress': 'Sales Order',
	'manual': 'Sale to Invoice',
} %>
%for o in objects:
<hr/>
<body>
<table width="100%">
	<tr>
		<td width="55%" style="vertical-align: top;">
			<table class="grey" width="100%" cellpadding="3px">
				<tr>
					<td class="grey" width="20%" style="background-color: #B65100; color: white; font-weight: bold;">Ship To</td>
					<td class="grey" width="80%">${o.partner_shipping_id.name}</td>
				</tr>
				<tr>
					<td class="grey" colspan="2">
						${o.partner_shipping_id.name} <br/>
						${o.partner_shipping_id.street or ''}<br/>
						${o.partner_shipping_id.city or ''},
						%if o.partner_shipping_id.state_id:
							${o.partner_shipping_id.state_id.code or ''}
						%endif
						${o.partner_shipping_id.zip or ''}
					</td>
				</tr>
			</table>
			<br/>
			<table class="grey" width="100%" cellpadding="3px">
				<tr>
					<td class="grey" width="20%" style="background-color: #B65100; color: white; font-weight: bold;">Bill To</td>
					<td class="grey" width="80%" colspan="2">${o.partner_invoice_id.name}</td>
				</tr>
				<tr>
					<td class="grey" colspan="3">
						${o.partner_invoice_id.name} <br/>
						${o.partner_invoice_id.street or ''}<br/>
						${o.partner_invoice_id.city or ''}, 
						%if o.partner_invoice_id.state_id:
							${o.partner_invoice_id.state_id.code or ''}
						%endif
						${o.partner_invoice_id.zip or ''} <br /><br />

						Phone: ${o.partner_invoice_id.phone or ''}<br />
						Email: ${o.partner_invoice_id.email or ''}<br />
						Fax: ${o.partner_invoice_id.fax or ''}<br />
					</td>
				</tr>
			</table>
		</td>
		<td width="45%"  style="vertical-align: top; padding-left: 20px">
			<table width="100%" class="content">
				<tr>
					<td class="content og_theme" width="35%">Date</td>
					<td class="content">${o.date_order and time.strftime('%b %d, %y', time.strptime( o.date_order,'%Y-%m-%d %H:%M:%S')) or ''}</td>
				</tr>
				<tr>
					<td class="content og_theme">Sales Person</td>
					<td class="content">${o.user_id and o.user_id.name|upper or ''}</td>
				</tr>
				<tr>
					<td class="content og_theme">Sales Order #</td>
					<td class="content">${o.name or ''}</td>
				</tr>
				<tr>
					<td class="content og_theme">Customer PO No</td>
					<td class="content">${o.client_order_ref or ''}</td>
				</tr>
<!-- 				<tr>
					<td class="content og_theme">Credit Card #</td>
					<td class="content">${o.partner_id.cc_number or ''}</td>
				</tr> -->
<!-- 				<tr>
					<td class="content og_theme">Exp. Date</td>
					<td class="content">${o.partner_id.cc_expiry_month and o.partner_id.cc_expiry_month.name or ''} ${o.partner_id.cc_expiry_year and o.partner_id.cc_expiry_year.name or ''}</td>
				</tr> -->
<!-- 				<tr>
					<td class="content og_theme">Name On CC</td>
					<td class="content">${o.partner_id.cc_name or ''}</td>
				</tr> -->
				<tr>
					<td class="content og_theme">PO #</td>
					<td class="content"></td>
				</tr>
				<tr>
					<td class="content og_theme">Agent Ref. PO #</td>
					<td class="content"></td>
				</tr>
				<tr>
					<td class="content og_theme">FED</td>
					<td class="content"></td>
				</tr>
				<tr>
					<td class="content og_theme">Taxable</td>
					<td class="content"></td>
				</tr>
				<tr>
					<td class="content og_theme">Payment Method</td>
					<td class="content">${o.payment_method and o.payment_method.name|upper or ''}</td>
				</tr>
				<tr>
					<td class="content og_theme">Status</td>
					<td class="content">${status[o.state]}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table class="content" width="100%" style="padding: 3px">
	<tr>
		<td class="content" style="text-align: center; font-weight: bold;">Line</td>
		<td class="content" style="text-align: center; font-weight: bold;">Vendor</td>
		<td class="content" style="text-align: center; font-weight: bold;">Vendor Part No.</td>
		<td class="content" style="text-align: center; font-weight: bold;">Discription</td>
		<td class="content" style="text-align: center; font-weight: bold;">Unit Cost</td>
		<td class="content" style="text-align: center; font-weight: bold;">Qty</td>		
		<td class="content" style="text-align: center; font-weight: bold;">Ext. Cost</td>
		<td class="content" style="text-align: center; font-weight: bold;">Unit Sale Price</td>
		<td class="content" style="text-align: center; font-weight: bold;">Ext. Price</td>
	</tr>
	<% set i=1 %>
	%for line in o.order_line:
	<tr style="page-break-inside: avoid;">
		<td class="content" style="text-align: center;">${i}</td>
		<td class="content">${line.purchase_id.partner_id and line.purchase_id.partner_id.name or 'NA'}</td>
		<td class="content hright">${formatLang(line.product_id.standard_price,digits=2)}</td>		
		<td class="content hmid">${formatLang(line.product_uom_qty,digits=0)}</td>
		<td class="content hright">${formatLang(line.product_id.standard_price * line.product_uom_qty,digits=2)}</td>
		<td class="content hright">${formatLang(line.price_unit,digits=2)}</td>
		<td class="content hright">${formatLang(line.price_unit * line.product_uom_qty,digits=2)}</td>
	</tr>
	<% set i=i+1 %>
	%endfor
	<tr>
		<td colspan="3"><b>Sub Total</b></td>
		<td colspan="1">${o.currency_id.symbol} ${formatLang(o.amount_untaxed,digits=2)}</td>
		<td colspan="3" style="border-left: 1px solid black"><b>Subtotal</b></td>
		<td colspan="2" class="hright">${o.currency_id.symbol} ${formatLang(o.amount_untaxed,digits=2)}</td>
	</tr>
	<tr>
		<td colspan="3"><b>Total Cost</b></td>
		<td colspan="3" style="border-left: 1px solid black"><b>Tax</b></td>
		<td colspan="2" class="hright">${o.currency_id.symbol} ${formatLang(o.amount_tax,digits=2)}</td>
	</tr>
	<tr>
		<td colspan="3"><b>Total Profit</b></td>
		<td></td>

		<td colspan="3" style="border-left: 1px solid black"><b>Shipping</b></td>
		<td colspan="2" class="hright"></td>
	</tr>
	<tr>
		<td colspan="3"></td>
		<td colspan="1"></td>
		<td colspan="3" rowspan="2" style="border-left: 1px solid black"><b>Total</b></td>
		<td colspan="2" rowspan="2" class="hright">${o.currency_id.symbol} ${formatLang(o.amount_total,digits=2)}</td>
	</tr>
	<tr>
		<td colspan="3"><b>Total Commission</b></td>

		<td></td>
	</tr>
</table>
<h3>Notes and Special Instructions</h3>
${o.note or ''}
<hr/>
<h3>Approval</h3>
<table width="100%" cellspacing="5px">
	<tr>
		<td width="35" class="box"></td>
		<td width="15" class="vmid" style="padding-left: 5px">Collections</td>
		<td width="35" class="box"></td>
		<td width="15" class="vmid" style="padding-left: 5px">General Manager</td>
	</tr>
	<tr>
		<td width="35" class="box"></td>
		<td width="15" class="vmid" style="padding-left: 5px">Supervisor</td>
		<td width="35" class="box"></td>
		<td width="15" class="vmid" style="padding-left: 5px">Purchasing</td>
	</tr>
</table>
</body>
%endfor
</html>