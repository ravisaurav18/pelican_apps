from openerp.addons.report_webkit import webkit_report
from openerp.addons.report_webkit.report_helper import WebKitHelper
from openerp.addons.report_webkit.webkit_report import webkit_report_extender
from openerp.report import report_sxw
from datetime import date
from datetime import datetime
import time
import unicodedata

class report_order_confirmation_pc(report_sxw.rml_parse):
	
	def __init__(self, cr, uid, name, context):
		super(report_order_confirmation_pc, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
								  'get_object': self._get_object,
								  'get_supplier_code': self._get_supplier_code1,
								  'get_supplier_prod_name': self._get_supplier_prod_name1,
								  'get_total_cost': self._get_total_cost1
								  })

	def _get_object(self,data):
		obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
		return obj_data

	def _get_supplier_code1(self,product,vendor):
		product_code = False
		for seller in [sellers for sellers in product.seller_ids if sellers.name.id == vendor.id]:
			product_code = seller.product_code
		return product_code

	def _get_supplier_prod_name1(self,product,vendor):
		product_name = False
		for seller in [sellers for sellers in product.seller_ids if sellers.name.id == vendor.id]:
			product_name = seller.product_name
		return product_name

	def _get_total_cost1(self, lines, order):
		cost = 0.0
		for line in lines:
			cost += (line.product_id.standard_price * line.product_uom_qty)
		# cost += order.amount_freight_cost
		# cost += order.cc_processing_fee
		return cost

webkit_report.WebKitParser('report.order.confirmation.pc', 'sale.order', 'pc_sale_report/report_template_pelican/report_order_confirmation_pc.mako', parser=report_order_confirmation_pc)
