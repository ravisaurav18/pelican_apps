<!DOCTYPE html>
<html>
<head>
	<title>Sales Order</title>
	<style>
		.break { page-break-after: always; }
		
		body
		{
			font-size: 12px;
			min-height: 100%;
		}

		table.no_border
		{
		border-collapse:collapse;
		padding:10px;
		width:100%;
		vertical-align: top;
		}
		
		table.grey
		{
		border-collapse:collapse;
		border: 1px solid #42f47a;
		padding:10px;
		width:100%;
		}

		td.grey
		{
		border: 1px solid LightGrey;
		text-align: center;
		}

		table.content
		{
		border-collapse:collapse;
		border: 1px solid black;
		}

		td.content
		{
		border-bottom: 1px solid LightGrey;
		vertical-align: top;
		}

		td.total
		{
		border-bottom: 1px solid black;
		vertical-align: top;
		font-weight: bold;
		}

		th.content
		{
		border-collapse: collapse;
		border-bottom: 1px solid black;
		}

		.footer 
		{
			position: absolute;
			right: 0;
			bottom: 0;
			left: 0;
			padding: 1rem;
			background-color: #efefef;
			text-align: center;
		}
		td.data
		{
		border-bottom:thin solid black;
		width:69%;
		}
		
		.var
		{
		font-weight:bold;
		}
		
		.left
		{
			text-align: left;
		}

		.center
		{
			text-align: center;
		}

		.right
		{
			text-align: right;
		}

		.padded
		{
		text-align:center;
		padding-top:30px;
		}
		
		.ttd
		{
		text-align:center;
		padding-bottom:80px;
		width:50%
		}
		
		.nama
		{
		text-align:center;
		width:50%;
		font-weight:bold;
		}
		
		
	</style>
</head>
%for o in objects:
<body>
 <table width="100%">
	<tr>
		<td style="font-size: 15px; vertical-align: middle;">
			Business Name: ${objects.company_id.name or ''} <br />
			${objects.company_id.street or ''} <br />
			${objects.company_id.city or ''}, ${objects.company_id.state_id and objects.company_id.state_id.code or ''} - ${objects.company_id.zip or '-'}<br />
			Phone: ${objects.company_id.phone or '-'} | Fax: ${objects.company_id.fax or '-'} <br />
<!-- 			DUNS: 185436586<br />
			FEIN (Federal Employer ID): 05-0608405 -->
		</td>
		<td style="text-align: right; vertical-align: middle;font-size: 15px;">
			%if o.state == 'draft' or o.state == 'sent':
			<b>Quotation: ${o.name}</b> <br/>
			Date: ${time.strftime('%b %d, %y', time.strptime( o.date_order,'%Y-%m-%d %H:%M:%S'))}
			%else:
			<b>Sales Order: ${o.name}</b> <br/>
			Date: ${time.strftime('%b %d, %y', time.strptime( o.date_order,'%Y-%m-%d %H:%M:%S'))}
			%endif
			<br/>
			
		</td>
	</tr>
</table><br/>
 
 <table width="100%" class="no_border" cellspacing="10px">
	<tr>
		<td width="40%" style="background-color: #B65100;padding-left: 10px; color: white;"><b>Sold to:</b></td>
		<td width="20%"></td>
		<td width="40%" style="background-color: #B65100;padding-left: 10px; color: white;"><b>Pick Up:</b></td>
	</tr>
	<tr>
		<td style="vertical-align: top;padding-left: 15px;">
			<b>${o.partner_id.name}</b> <br />
			${o.partner_id.street or ''} <br />
			%if o.partner_id.street2:
			${o.partner_id.street2 or ''} <br />
			%endif
			${o.partner_id.city or ''} ${o.partner_id.state_id and o.partner_id.state_id.code or ''} ${o.partner_id.zip or ''} <br />
			<b>Phone: </b> ${o.partner_id.phone or ''} <br />
			<b>Fax: </b> ${o.partner_id.fax or ''} <br />
		</td>
		<td></td>
		<td style="vertical-align: top;padding-left: 15px;">
			<b>${o.partner_shipping_id.name}</b> <br />
			${o.partner_shipping_id.street or ''} <br />
			%if o.partner_shipping_id.street2:
			${o.partner_shipping_id.street2 or ''} <br />
			%endif
			${o.partner_shipping_id.city or ''} ${o.partner_shipping_id.state_id and o.partner_shipping_id.state_id.code or ''} ${o.partner_shipping_id.zip or ''} <br />
			<b>Phone: </b> ${o.partner_shipping_id.phone or ''} <br />
			<b>Fax: </b> ${o.partner_shipping_id.fax or ''} <br />
		</td>
	</tr>
</table><br />
<table class="grey" cellpadding="10px">
	<tr>
		<td width="25%" class="grey">
			<b>Salesperson:</b> <br />
			${o.user_id.name or ' '}
		</td>
		<td width="25%" class="grey">
			<b>Reference Number:</b> <br />
			${o.client_order_ref or '-'}
		</td>
		<td width="25%" class="grey">
			<b>Release Number:</b> <br />
			${o.client_order_ref or '-'}
		</td>		
<!-- 		<td width="25%" class="grey">
			<b>Ship Via:</b> <br />
			${o.ship_via or '-'}
		</td> -->
<!-- 		<td width="25%" class="grey">
			<b>Freight:</b> <br />
			&nbsp;
		</td> -->
	</tr>
</table><br />

<table width="100%" cellspacing="0" cellpadding="2px">
	<tr>
		<th width="5%" class="content" colspan="1" style="text-align: left;">No</th>
		<th width="70%" class="content" colspan="1" style="text-align: left;">Description</th>
		<th width="5%" class="content" colspan="1" style="text-align: right;">Qty</th>
		<th width="10%" class="content" colspan="2" style="text-align: right;">Unit Price</th>
		<th width="10%" class="content" colspan="1" style="text-align: right;">Price</th>
	</tr>
	<% set i=1 %>
	%for line in o.order_line:
	<tr style="page-break-inside: avoid;">
		<td class="content" colspan="1" style="text-align: left; vertical-align: top;">${i}</td>
		<td class="content" colspan="1" style="text-align: left; vertical-align: top;">
			${line.name|safe} <br/>
			%if line.product_id.image and line.print_product_image:
			<img src="data:image/png;base64,${line.product_id.image}" height="100px" />
			%endif
		</td>
		<td class="content" colspan="1" style="text-align: right; vertical-align: top;">${line.product_uom_qty}</td>
		<td class="content" colspan="2" style="text-align: right; vertical-align: top;">${o.currency_id.symbol} ${formatLang(line.price_unit,digits=2)}</td>
		<td class="content" colspan="1" style="text-align: right; vertical-align: top;">${o.currency_id.symbol} ${formatLang(line.price_subtotal,digits=2)}</td>
	</tr>
	<% set i=i+1 %>
	%endfor
	<tr>
		<td colspan="2"></td>
		<td colspan="3" class="total">Total Without Taxes</td>
		<td colspan="1" class="total" style="text-align: right; vertical-align: top;">${o.currency_id.symbol} ${formatLang(o.amount_untaxed,digits=2)}</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td colspan="3" class="content">Taxes</td>
		<td colspan="1" class="content" style="text-align: right; vertical-align: top;">${o.currency_id.symbol} ${formatLang(o.amount_tax,digits=2)}</td>
	</tr>
	<tr>
		<td colspan="2"></td>
		<td colspan="3" class="total">Total</td>
		<td colspan="1" class="total" style="text-align: right; vertical-align: top;">${o.currency_id.symbol} ${formatLang(o.amount_total,digits=2)}</td>
	</tr>
</table> 
%if o.note:
<p>
	<b>Terms and Conditions:</b> <br />
	<pre>${o.note}</pre>
</p>
%endif


<br /><br />
<br /><br />
 <table width="100%">
	<tr>
		<td style="font-size: 10px; vertical-align: middle;">
			PLEASE BE ADVISED:<br />
			* Payment must be received within 48 hours<br />
			* All containers are sold as is where is<br />
			* All containers are to be picked up within 7 days of receiving your release number <br />
		</td>
	</tr>
</table><br/>

<br /><br />
${o.user_id.signature|safe or ''}

</body>
%endfor
</html>