from openerp.addons.report_webkit import webkit_report
from openerp.addons.report_webkit.report_helper import WebKitHelper
from openerp.addons.report_webkit.webkit_report import webkit_report_extender
from openerp.report import report_sxw
from datetime import date
from datetime import datetime
import time
import unicodedata
import base64

class template_sale_order_pc(report_sxw.rml_parse):
	
	def __init__(self, cr, uid, name, context):
		super(template_sale_order_pc, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
								  'get_object'      : self._get_object,
								  'image_data'		: self._image_data
								  })

	def _get_object(self,data):
		obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
		return obj_data

	def _image_data(self, product):
		return base64.b64encode(product)


webkit_report.WebKitParser('report.sale.order.pc', 'sale.order', 'wagon_sale/report_template_pelican/report_sale_order_pc.mako', parser=template_sale_order_pc)
