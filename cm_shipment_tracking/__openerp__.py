# -*- coding: utf-8 -*-

{
    'name': 'Common Shipment Tracking',
    'summary': ''' ''',
    'images': [],
    'author': 'RSNT',
    'website': '',
    'category': 'Purchase Management',
    'version': '9.0.1.1',
    'license': '',
    'depends': ['purchase','delivery'
             ],
    'data': [
        'views/shipment_tracking.xml',
        'views/purchase_view.xml', 
        #'views/sale_view.xml',                  
        'security/ir.model.access.csv',
                
    ],

    'images': ['static/description/icon.png'],    
    
}
