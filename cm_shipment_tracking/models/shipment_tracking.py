from openerp import models,fields,api,_
from openerp.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)


class PurchaseShipmentTracking(models.Model):
    _name = 'shipment.tracking'    

    purchase_no=fields.Many2one('purchase.order',string="PO")
    product_id=fields.Many2one(related='purchase_no.product_id',string="Product Name")
    tracking_no=fields.Char(string="Tracking No")
    tracking_carrier=fields.Many2one('delivery.carrier',string="Delivery Carrier")    
    tracking_status=fields.Selection([('transit', 'Transit'),('delivered', 'Delivered'),('unavailable', 'Unavailable')],string='Status')
          
