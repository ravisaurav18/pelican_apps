from openerp import models,fields,api,_
from openerp.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)


class PurchaseShippingTrack(models.Model):
    _inherit = 'purchase.order'    

    purchase_tracking_ids=fields.One2many('shipment.tracking','purchase_no',string="Purchase-Tracking")


  