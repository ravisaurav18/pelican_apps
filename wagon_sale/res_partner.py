from openerp.osv import osv,fields
from difflib import SequenceMatcher
from openerp.tools.translate import _
from openerp.exceptions import except_orm, Warning
from openerp import SUPERUSER_ID

class res_month(osv.osv):
	_name = "res.month"
	_description = "Month"
	_columns = {
		'name': fields.char('Name'),
	}

class res_year(osv.osv):
	_name = "res.year"
	_description = "Year"
	_columns = {
		'name': fields.char('Name'),
	}

# class res_partner(osv.osv):
# 	_inherit = "res.partner"
# 	_columns = {
# 		'cc_name': fields.char('Name',size=100),
# 		'cc_number': fields.char('Card Number',size=16),
# 		'cc_expiry_month': fields.many2one('res.month','Month'),
# 		'cc_expiry_year': fields.many2one('res.year','Year'),
# 		'cc_cvv': fields.char('CVV',size=3),
# 		'cc_type': fields.selection([('visa','Visa'),('master','MasterCard')],'Type'),
# 		'account_code': fields.char('Account ID',size=10),
# 		'facebook': fields.char('Facebook',size=500),
# 		'twitter': fields.char('Twitter',size=500),
# 		'linkedin': fields.char('Linked-In',size=500),
# 		'google': fields.char('Google+',size=500)
# 	}
