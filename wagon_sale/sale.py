from openerp.osv import osv,fields
from openerp.tools.translate import _

class sale_order(osv.osv):
	_inherit = "sale.order"
	_columns = {
		'purchase_order_ids': fields.one2many('purchase.order','sale_order_id','Purchase Order'),
		'payment_method': fields.many2one('account.payment.method','Payment Method'),			
		'tax_ids': fields.many2many('account.tax','order_tax_rel','order_id','tax_id','Taxes'),
	}
	def action_quotation_send(self, cr, uid, ids, context=None):
		'''
		This function opens a window to compose an email, with the edi sale template message loaded by default
		'''
		assert len(ids) == 1, 'This option should only be used for a single id at a time.'
		ir_model_data = self.pool.get('ir.model.data')
		try:
			template_id = ir_model_data.get_object_reference(cr, uid, 'sale', 'email_template_edi_sale')[1]
		except ValueError:
			template_id = False
		try:
			compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
		except ValueError:
			compose_form_id = False 
		ctx = dict()
		mail_server = self.pool.get('ir.mail_server').search(cr,uid,[('owner_user_id','=',uid)])
		ctx.update({
			'default_model': 'sale.order',
			'default_res_id': ids[0],
			'default_use_template': bool(template_id),
			'default_template_id': template_id,
			'default_composition_mode': 'comment',
			'mark_so_as_sent': True,

			'default_mail_server_id': mail_server and mail_server[0] or False
		})
		return {
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'mail.compose.message',
			'views': [(compose_form_id, 'form')],
			'view_id': compose_form_id,
			'target': 'new',
			'context': ctx,
		}

	def copy_quotation(self, cr, uid, ids, context=None):
		order = self.browse(cr,uid,ids[0],context=None)
		fixed_number = order.name


		if order.original_order:
			original_number = order.original_order.name
			next_cancel_number = int(original_number.split('/')[-1])+1
			self.write(cr,uid,ids,{'name': fixed_number+'/'+str(next_cancel_number)})
		else:
			original_number = order.name
			self.write(cr,uid,ids,{'name': fixed_number+'/1'})

		id = self.copy(cr, uid, ids[0], {'name':fixed_number, 'original_order': ids[0]}, context=context)
		view_ref = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'sale', 'view_order_form')
		view_id = view_ref and view_ref[1] or False,
		return {
			'type': 'ir.actions.act_window',
			'name': _('Sales Order'),
			'res_model': 'sale.order',
			'res_id': id,
			'view_type': 'form',
			'view_mode': 'form',
			'view_id': view_id,
			'target': 'current',
			'nodestroy': True,
		}

	def apply_tax(self, cr, uid, ids, context=None):
		tax_ids = []
		for order in self.browse(cr,uid,ids,context=context):
			for tax in order.tax_ids:
				tax_ids.append(tax.id)
			for line in order.order_line:
				if tax_ids:
					self.pool.get('sale.order.line').write(cr,uid,line.id,{'tax_id':[(6,0,[tax_ids])]})
				else:
					self.pool.get('sale.order.line').write(cr,uid,line.id,{'tax_id':[(5)]})
		return True

class sale_order_line(osv.osv):
	_inherit = "sale.order.line"
	_columns = {
		'ordered': fields.boolean('Ordered'),
		'purchase_id': fields.many2one('purchase.order','PO'),
		'purchase_line_id': fields.many2one('purchase.order.line','PO Line'),
		'print_product_image': fields.boolean('Print Product Image'),
		'product_desciption': fields.html("Product Description for Pricing Team"),	
		'product_image': fields.binary('Product Image'),		
	}
	_defaults = {
		'print_product_image': True
	}

class sale_advance_payment_inv(osv.osv_memory):
	_inherit = "sale.advance.payment.inv"

	_columns = {
		'advance_payment_method':fields.selection(
			[('all', 'Full sales'), ('percentage','Percentage'), ('fixed','Fixed price (deposit)'),
				('lines', 'Some order lines')],
			'What do you want to invoice?', required=True,
			help="""Use Invoice the whole sale order to create the final invoice.
				Use Percentage to invoice a percentage of the total amount.
				Use Fixed Price to invoice a specific amound in advance.
				Use Some Order Lines to invoice a selection of the sales order lines."""),
	}
