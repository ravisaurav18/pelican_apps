from openerp.osv import osv,fields

class crm_lead(osv.osv):
	_inherit = "crm.lead"
	_columns = {
		'facebook': fields.char('Facebook',size=500),
		'twitter': fields.char('Twitter',size=500),
		'linkedin': fields.char('Linked-In',size=500),
		'google': fields.char('Google+',size=500),
		'website': fields.char('Website',size=500),
		'license_type': fields.selection([('mc','MC')],'License Type'),
		'license_number': fields.char('License Number'),
		'business_type_id': fields.many2one('crm.business.type','Business Type')
	}

	def takeover(self, cr, uid, ids, context={}):
		self.write(cr,uid,ids,{'user_id':uid})
		return True

	def mark_as_closed(self, cr, uid, ids, context={}):
		self.write(cr,uid,ids,{'active':False})
		return True

class crm_business_type(osv.osv):
	_name = "crm.business.type"
	_description = "Business Type"
	_columns = {
		'name': fields.char('Name')
	}