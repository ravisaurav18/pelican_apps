from openerp.osv import fields, osv

class sale_config_settings(osv.osv_memory):
	_inherit = 'sale.config.settings'
	_columns = {
		'lead_followup_delay': fields.integer("Days allowed"),
	}
	def default_get(self, cr, uid, fields, context=None):
		res = super(sale_config_settings, self).default_get(cr, uid, fields, context)
		user = self.pool.get('res.users').browse(cr, uid, uid, context)
		res['lead_followup_delay'] = user.company_id.lead_followup_delay
		return res

	def set_lead_followup_delay(self, cr, uid, ids, context=None):
		config = self.browse(cr, uid, ids[0], context)
		user = self.pool.get('res.users').browse(cr, uid, uid, context)
		user.company_id.write({'lead_followup_delay': config.lead_followup_delay})
