{
		"name"          : "Common Customized Sale & CRM",
		"version"       : "1.0",
		"depends"       : ["base","sale","purchase","wagon_mail",'crm','report_webkit'],
		"author"        : "RSNT",
		"description"   : """
This module has customized several aspects in Sales & CRM module, including:

- Quotation / Order template
- Leads Management
""",
		"website"       : "",
		"category"      : "Sales",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"data.xml",
							"wizard/sale2po_view.xml",							
							"security/sale_security.xml",
							"security/sale_report_security.xml",							
							"security/ir.model.access.csv",
							"sale_view.xml",
							"purchase_view.xml",
							#"res_partner_view.xml",
							#"res_config_view.xml",
							#"res_company_view.xml",
							#"report/report_sale_order.xml",
							# "report_template_pelican/report_sale_order_pelican.xml",
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}