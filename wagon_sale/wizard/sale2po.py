from openerp.osv import osv,fields
from openerp.tools.translate import _

class sale2po_line_wizard(osv.osv_memory):
	_name = "sale2po.line.wizard"
	_columns = {
		'wizard_id': fields.many2one('sale2po.wizard','Wizard'),
		'order_line_id': fields.many2one('sale.order.line','SO Line'),
		'product_id': fields.many2one('product.product','Product'),
		'name': fields.char('Description',size=200),
		'price_unit': fields.float('Price Unit'),
		'product_qty': fields.float('Quantity'),
		'selected_vendor_from_vendorlist':fields.char('Approved Vendor',readonly=True),
	}

class sale2po_wizard(osv.osv_memory):
	_name = "sale2po.wizard"
	_columns = {
		'partner_id': fields.many2one('res.partner', 'Vendor', required=True, domain=[('supplier','=',True)]),
		'line_ids': fields.one2many('sale2po.line.wizard','wizard_id','Lines'),
		'transaction_charges': fields.float('Transaction Charges'),
	}

	def default_get(self, cr, uid, fields, context=None):
		if context is None: context = {}
		res = super(sale2po_wizard, self).default_get(cr, uid, fields, context=context)
		active_ids = context.get('active_ids')
		
		items = []
		for sale in self.pool.get('sale.order').browse(cr,uid,active_ids):
			for line in [lines for lines in sale.order_line if not lines.purchase_line_id]:
				item = {
					'order_line_id': line.id,
					'product_id':line.product_id.id,
					'name': line.product_id and line.product_id.name or "",
					'price_unit': line.product_id.standard_price,
					'product_qty':line.product_uom_qty,
					'selected_vendor_from_vendorlist':line.product_id.selected_vendor_from_vendorlist or False,
				}
				items.append((0,0,item))
		res.update(line_ids=items)
		return res


	# def _get_product(self,cr,uid,ids,context={}):
	# 	result = {}
	# 	for partner in self.pool.get('res.partner').browse(cr, uid, ids, context=context):
	# 		product_id = self.pool.get('product.template').search(cr,uid,[('product_id','=',partner.product_id)])
	# 	return product_id    	

	def create_po(self,cr,uid,ids,context={}):
		print "CONTEXT",context
		for w in self.browse(cr,uid,ids,context):
			print "1"
			order = self.pool.get('sale.order').browse(cr,uid,context['active_id'])
			
			if not w.line_ids:
				raise osv.except_osv(_('Warning!'), _('You already ordered all items in this SO.'))
			location = self.pool.get('stock.location').search(cr, uid, [('usage','=','internal')])
			picking = self.pool.get('stock.picking.type').search(cr, uid, [('code','=','incoming')])
			warehouse = self.pool.get('stock.warehouse').search(cr, uid, [])
			po_vals = {
				'partner_id': w.partner_id.id,
				'location_id': location and location[0] or False,
				#'pricelist_id': w.partner_id.property_product_pricelist_purchase and w.partner_id.property_product_pricelist_purchase.id or False,
				'picking_type_id': picking and picking[0] or False,
				'minimum_planned_date': fields.datetime.now(),
				'origin': order.name,
				'sale_order_id': order.id,
				'dest_address_id': order.partner_shipping_id and order.partner_shipping_id.id or False,
				'customer_address':"updating soon",
				'salesman_id': order.user_id.id,
			}
			print "3"
			po = self.pool.get('purchase.order').create(cr, uid, po_vals, context)

			for line in w.line_ids:
				po_line_vals = {
					'product_id': line.product_id and line.product_id.id or False,
					'name': line.product_id and line.product_id.name or False,
					'date_planned': fields.datetime.now(),
					'product_qty': line.product_qty,
					'product_uom': line.product_id.uom_id and line.product_id.uom_id.id or False,
					'price_unit': line.price_unit,
					'salesman_id': line.order_line_id.order_id.user_id and line.order_line_id.order_id.user_id.id,
					'sale_line_id': line.order_line_id.id,
					'order_id': po,
				}
				po_line = self.pool.get('purchase.order.line').create(cr, uid, po_line_vals, context)
				self.pool.get('sale.order.line').write(cr,uid,line.order_line_id.id,{'ordered':True,'purchase_id':po,'purchase_line_id':po_line})

			print "4"
		return True