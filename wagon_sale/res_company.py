from openerp.osv import fields, osv

class res_company(osv.osv):
	_inherit = "res.company"
	_columns = {
		'lead_followup_delay': fields.integer('Leads Follow up Delay'),
	}