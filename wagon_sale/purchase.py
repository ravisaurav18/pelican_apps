from openerp import models,fields,api,_
import openerp.addons.decimal_precision as dp

class purchase_order(models.Model):
	_inherit = "purchase.order"

	sale_order_id=fields.Many2one('sale.order',string='SO Reference')
	transaction_charges=fields.Float('Transaction Charges')


class purchase_order_line(models.Model):
	_inherit = "purchase.order.line"
	sale_line_id=fields.Many2one('sale.order.line',string='Sale Line ID')
	#discount = fields.Float(string='Discount (%)', digits=dp.get_precision('Discount'), default=0.0)

	# @api.depends('order_line.price_total')
	# def _amount_all(self):
	# 	"""
	# 	Compute the total amounts of the SO.
	# 	"""
	# 	for order in self:
	# 		amount_untaxed = amount_tax = 0.0
	# 		for line in order.order_line:
	# 			amount_untaxed += line.price_subtotal
	# 			# FORWARDPORT UP TO 10.0
	# 			if order.company_id.tax_calculation_rounding_method == 'round_globally':
	# 				price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
	# 				taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
	# 				amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
	# 			else:
	# 				amount_tax += line.price_tax
	# 		order.update({
	# 			'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
	# 			'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
	# 			'amount_total': amount_untaxed + amount_tax,
	# 		})

	# @api.depends('product_qty', 'discount', 'price_unit', 'taxes_id')
	# def _compute_amount(self):
	# 	"""
	# 	Compute the amounts of the SO line.
	# 	"""
	# 	for line in self:
	# 		price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
	# 		taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_qty, product=line.product_id, partner=line.order_id.partner_id)
	# 		line.update({
	# 			'price_tax': taxes['total_included'] - taxes['total_excluded'],
	# 			'price_total': taxes['total_included'],
	# 			'price_subtotal': taxes['total_excluded'],
	# 		})            