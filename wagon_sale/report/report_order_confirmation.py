from openerp.addons.report_webkit import webkit_report
from openerp.addons.report_webkit.report_helper import WebKitHelper
from openerp.addons.report_webkit.webkit_report import webkit_report_extender
from openerp.report import report_sxw
from datetime import date
from datetime import datetime
import time
import unicodedata

class report_order_confirmation(report_sxw.rml_parse):
	
	def __init__(self, cr, uid, name, context):
		super(report_order_confirmation, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
								  'get_object': self._get_object,
								  })

	def _get_object(self,data):
		obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
		return obj_data



webkit_report.WebKitParser('report.order.confirmation', 'sale.order', 'wagon_sale/report/report_order_confirmation.mako', parser=report_order_confirmation)
