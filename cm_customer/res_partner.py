#from openerp.osv import osv,fields
from openerp import models,fields,api,_
from difflib import SequenceMatcher
from openerp.exceptions import UserError
from openerp.tools.translate import _
from openerp.exceptions import ValidationError
from openerp.exceptions import except_orm, Warning
from openerp import SUPERUSER_ID
import re

class VerifyPartner(models.Model):
	_description = "Verify Partner"
	_inherit = "res.partner"


	@api.model
	def _get_default_team(self):
		default_team_id = self.env['crm.team']._get_default_team_id()
		return self.env['crm.team'].browse(default_team_id)

	partner_verified=fields.Boolean(string='Verified Customer',readonly=True,track_visibility="onchange")
	state=fields.Selection(
	        [
	            ('new', 'New'),
	            ('verified', 'Verified'),
	        ], 'State',
	        readonly=True, select=True, copy=False,
	        default='new')
	customer_type=fields.Selection([('us-government','US-Government'),('foreign-government','Foreign-Government'),('business','Business'),('private','Private'),('unknown','Unknown')], 'Customer Type') #added by ravi
	team_id=fields.Many2one('crm.team',string='Sales Team',default=_get_default_team, oldname='section_id',track_visibility="onchange")
	#user_id=fields.Many2many('res.users', 'res_groups_users_rel', 'gid', 'uid', string='Salesperson')

	
	_sql_constraints = [('email_uniq', 'unique(email)', 'Email id is unique change your custom email id'),
						('phone_uniq','unique(phone)','Phone number is unique so change your phone number'),
						('name_uniq','unique(phone)','Name is unique so change your Customer Name')]

	@api.multi
	def verify_customer(self):
		for record in self:
			# check that phone no is not empty and its not repeated.
			if not record.phone:
				raise UserError(_("Please Fill Customer Phone"))	
			# check that Salesperson is not empty.			
			if not record.user_id:
				raise UserError(_("No SalesPerson found for this Customer!!!"))					
			# check that SalesTeam is not empty.			
			if not record.team_id:
				raise UserError(_("No Sales Team found for this Customer!!!"))	
			# check that Customer Type is not empty							
			if not record.customer_type:
				raise UserError(_("Please Fill Customer Type"))
			# check that email is not empty and its not repeated.
			if not record.email:
				raise UserError(_("Please Fill Customer Email"))
					
		self.partner_verified=True
		self.state='verified'           	
		return True

	@api.constrains('phone')
	def check_phone_no(self):
		if self.phone and len("%s"%(self.phone))<8:
			raise Warning("Incorrect Phone No.")

	@api.multi
	def cancel_verify_customer(self):
		self.partner_verified=True
		self.state='new'   
		return True		


	# @api.model
	# def create(self,vals):
	# 	#user=self.pool.get('res.users').browse(cr,uid,uid,context=None)
	# 	user=self.env['res.users'].search([])
	# 	#vals['section_id']=user.default_section_id and user.default_section_id.id or False
	# 	#all_customers = self.search(cr,SUPERUSER_ID,[('customer','=',True),('active','=',True)])

	# 	all_customers = self.env['res.partner'].search([('customer','=',True),('active','=',True)])
	# 	for customer in all_customers:
	# 		if vals.get('name',False):
	# 			similarity = SequenceMatcher(None, vals['name'].upper().strip(), customer.name.upper().strip()).ratio()
	# 			if similarity >= 0.90:
	# 				raise UserError(_('Customer Already Exist!..There is one (or more) customer that'\
	# 													' has similar name \n Please contact your Sales Manager'))
	# 		if vals.get('phone',False) and customer.phone:
	# 			similarity = SequenceMatcher(None, vals['phone'], customer.phone).ratio()
	# 			if similarity >= 0.90:
	# 				raise UserError(_('Customer Already Exist!..There is one (or more) customer that'\
	# 													' has similar name \n Please contact your Sales Manager'))
	# 		if vals.get('email',False) and customer.email:
	# 			similarity = SequenceMatcher(None, vals['email'].upper().strip(), customer.email.upper().strip()).ratio()
	# 			if similarity >= 0.90:
	# 				raise UserError(_('Customer Already Exist!..There is one (or more) customer that'\
	# 													' has similar name \n Please contact your Sales Manager'))
	# 	return super(VerifyPartner,self).create(vals)

	def create(self,cr,uid,vals,context=None):
		user=self.pool.get('res.users').browse(cr,uid,uid,context=None)
		# vals['section_id']=user.default_section_id and user.default_section_id.id or False
		all_customers = self.search(cr,SUPERUSER_ID,[('customer','=',True),('active','=',True)])
		for customer in self.browse(cr,SUPERUSER_ID,all_customers):
			if vals.get('name',False):
				similarity = SequenceMatcher(None, vals['name'].upper().strip(), customer.name.upper().strip()).ratio()
				if similarity >= 0.85:
					raise except_orm(_('Customer Already Exist!'), _('There is one (or more) customer that'\
												' has similar name.\n Please contact your Sales Manager'))
			if vals.get('phone',False) and customer.phone:
				similarity = SequenceMatcher(None, vals['phone'], customer.phone).ratio()
				if similarity >= 0.85:
					raise except_orm(_('Customer Already Exist!'), _('There is one (or more) customer that'\
												' has similar phone number.\n Please contact your Sales Manager'))
			if vals.get('email',False) and customer.email:
				similarity = SequenceMatcher(None, vals['email'].upper().strip(), customer.email.upper().strip()).ratio()
				if similarity >= 0.85:
					raise except_orm(_('Customer Already Exist!'), _('There is one (or more) customer that'\
												' has similar email.\n Please contact your Sales Manager'))
		return super(VerifyPartner,self).create(cr,uid,vals,context=None)

	# @api.multi
	# def write(self,vals):
	# 	super(VerifyPartner,self).write(vals)
	# 	#all_customers = self.search(cr,SUPERUSER_ID,[('customer','=',True)])
	# 	all_customers = self.env['res.partner'].search([('customer','=',True),('active','=',True)])
	# 	#for data in self.browse(cr,uid,ids):
	# 	for data in self:		
	# 		if data.id in all_customers: 
	# 			all_customers.remove(data.id)
	# 		if data.customer:
	# 			for customer in all_customers:
	# 				if vals.get('name',False):
	# 					similarity = SequenceMatcher(None, vals['name'].upper().strip(), customer.name.upper().strip()).ratio()
	# 					if similarity >= 0.80:
	# 						raise UserError(_('Customer Already Exist!..There is one (or more) customer that'\
	# 															' has similar name \n Please contact your Sales Manager'))
	# 				if vals.get('phone',False) and customer.phone:
	# 					similarity = SequenceMatcher(None, vals['phone'], customer.phone).ratio()
	# 					if similarity >= 0.80:
	# 						raise UserError(_('Customer Already Exist!..There is one (or more) customer that'\
	# 															' has similar name \n Please contact your Sales Manager'))
	# 				if vals.get('email',False) and customer.email:
	# 					similarity = SequenceMatcher(None, vals['email'].upper().strip(), customer.email.upper().strip()).ratio()
	# 					if similarity >= 0.80:
	# 						raise UserError(_('Customer Already Exist!..There is one (or more) customer that'\
	# 															' has similar name \n Please contact your Sales Manager'))
	# 	return True
