{
		"name"          : "Common Customized Customer",
		"version"       : "1.0",
		"depends"       : ["base","crm"],
		"author"        : "RSNT",
		"description"   : """
						  This module will Verify Customer by Admin that Customer is Valid or Not.
						  """,
		"website"       : "",
		"category"      : "Partner",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"partner_view.xml",
							"security/ir_rule.xml",
							"sales_view.xml",
							],
		"active"        : True,
		"application"   : True,
		"installable"   : True,
}