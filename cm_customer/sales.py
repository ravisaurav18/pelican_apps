from openerp import models,fields,api,_
from openerp.exceptions import UserError

class CustomerSaleExtended(models.Model):
	_inherit = "sale.order"
	
	customer_type=fields.Selection(related="partner_id.customer_type",string="Customer Type")

