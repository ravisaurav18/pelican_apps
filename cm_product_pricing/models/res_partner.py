from openerp import models,fields,api,_
from openerp.exceptions import UserError

class VerifyVendor(models.Model):
    _inherit="res.partner"
      	
    vendor_verified=fields.Boolean(string='Verified Vendor',readonly=True,track_visibility="onchange",)
    vendor_state=fields.Selection(
            [
                ('new', 'New'),
                ('verified', 'Verified'),
            ], 'State',
            readonly=True, select=True, copy=False,
            default='new')

    @api.multi
    def verify_vendor(self):
        for record in self:
            # check that email is not empty and its not repeated
            if not record.email:
                raise UserError(_("Please Fill Vendor Email"))
            # check that phone no is not empty and its not repeated
            if not record.phone:
                raise UserError(_("Please Fill Vendor Phone"))        
        self.vendor_verified=True
        self.vendor_state='verified'               
        return True


    @api.multi
    def cancel_verify_vendor(self):
        self.vendor_verified=True
        self.vendor_state='new'   
        return True     