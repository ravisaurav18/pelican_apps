from openerp import models,fields,api,_
from openerp.exceptions import UserError

class CmSale(models.Model):
	_inherit = "sale.order"
	
	ship_to_zip=fields.Char(related="partner_shipping_id.zip",track_visibility="onchange",string='Shipping Zipcode')
	ship_to_city=fields.Char(related="partner_shipping_id.city",track_visibility="onchange",string='Shipping Zipcode')
	#customer_type=fields.Selection(related="partner_id.customer_type",string="Customer Type")

	@api.multi
	def _update_zip_code(self):
		#product_obj=self.env['product.template']
		# self.customer_type = self.partner_id.customer_type
		for products in self.order_line:
			for product in products.product_id:
				if product.type !='service':
					product.dropship_zipcode = self.partner_shipping_id.zip
					product.shipping_state = self.partner_shipping_id.city
					product.shipping_country = self.partner_shipping_id.country_id.name	
					product.product_desciption = products.product_desciption	
					#product.product_customer_type = self.partner_shipping_id.customer_type	or self.partner_id.customer_type or False					
		return True


	@api.multi
	def write(self, vals):
		res = super(CmSale, self).write(vals)
		self._update_zip_code()
		return res
