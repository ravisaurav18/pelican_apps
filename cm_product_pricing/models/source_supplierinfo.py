from openerp import models,fields,api,_
from openerp.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)


class SourceSupplierInfo(models.Model):
    _name = 'product.supplierinfo'    
    _inherit = ['mail.thread','product.supplierinfo','ir.needaction_mixin']
    _order = 'status,price,dropshipping_charge asc'
    # _order = 'price asc'
    state=fields.Selection([('draft','New'),
							('inquiry_sent', 'Inquiry Sent'),
							('inquiry_received','Reply Received')],string="Inquiry", copy=False,default='draft')

    status=fields.Selection([('underprocess','In Progress'),
							('rejected', 'Rejected'),
							('accepted','Accepted'),
							('blacklist','Black Listed')], copy=False,default='underprocess',track_visibility="onchange",)

    dropshipping_charge=fields.Float('Dropship Charge',track_visibility="onchange")  
    dropship_zipcode=fields.Char(related='product_tmpl_id.dropship_zipcode',string='Dropship Zip',track_visibility="onchange")

    #vendor_product_live_status=fields.Many2many('vendor.tag','vendor_tag_rel','supplierinfo_id', 'tag_id',string='Product live status',  help="This is the Current Status of Product given by Vendor.")  
    #vendor_shipping_live_status=fields.Many2many('vendor.tag','vendor_tag_rel','supplierinfo_id','tag_id',string='Dropship live status', help="This is the Current Status of Dropship given by Vendor.")
    
    vendor_product_live_status=fields.Many2many('vendor.tag',string='Product live status',  help="This is the Current Status of Product given by Vendor.")  
    vendor_shipping_live_status=fields.Many2many('vendor.shipping.tag',string='Dropship live status', help="This is the Current Status of Dropship given by Vendor.")

class vendor_tag(models.Model):
    _name = 'vendor.tag'

    name=fields.Char(string='Name', required=True)
    color=fields.Integer(string='Color Index')
    #vendor_id=fields.Many2many('product.supplierinfo','vendor_tag_rel','tag_id' ,'supplierinfo_id', string='Vendor List')

class vendor_shipping_tag(models.Model):
    _name = 'vendor.shipping.tag'

    name=fields.Char(string='Name', required=True)
    color=fields.Integer(string='Color Index')    