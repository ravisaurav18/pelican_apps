from openerp import models,fields,api, _
import logging
from datetime import datetime, date, timedelta
from openerp.exceptions import UserError
import time
import math
import openerp.addons.decimal_precision as dp

AVAILABLE_PRIORITIES = [
    ('1', 'Low'),
    ('2', 'Meduim'),
    ('3', 'High'),
    ('4', 'Urgent')
]

class SourceProduct(models.Model):
    
    _inherit='product.template'

    # @api.multi
    # def write(self, vals):
    #     res = super(SourceProduct, self).write(vals)
    #     if 'seller_ids' in vals:
    #         line_ids = self.seller_ids.filtered(lambda x: x.status).sorted(key=lambda l: (l.status,l.price,l.dropshipping_charge))
    #         for rec in self:
    #             rec.standard_price_from_vendor = line_ids[0].price
    #             rec.shipping_cost = line_ids[0].dropshipping_charge
    #             rec.standard_price=line_ids[0].price + line_ids[0].dropshipping_charge
    #     return res


    @api.multi
    def write(self, vals):
        res = super(SourceProduct, self).write(vals)
        if 'seller_ids' in vals:
            for obj in self.seller_ids:                   
                if obj.status == 'accepted': 
                    line_ids = self.seller_ids.filtered(lambda x: x.status).sorted(key=lambda l: (l.status,l.price,l.dropshipping_charge))
                    for rec in self:
                        rec.standard_price_from_vendor = line_ids[0].price
                        rec.shipping_cost = line_ids[0].dropshipping_charge
                        rec.standard_price=line_ids[0].price + line_ids[0].dropshipping_charge
                    break
                if obj.status == 'underprocess':
                    for rec in self:
                        rec.standard_price_from_vendor = 0.00
                        rec.shipping_cost = 0.00
                        rec.standard_price= 0.00                    
        return res

    def _referencable_models(self):
        models = self.env['res.request.link'].search([])
        return [(x.object, x.name) for x in models]

    @api.model
    def create(self, vals):
        if self.type == 'service':
            vals['is_source_product'] = False
        else:
            vals['is_source_product'] = True
        return super(SourceProduct, self).create(vals)

    # class effeciency(models.Model):
    #     _name= "effeciency.effeciency"

    # def _get_min_test(self):
    #     recs = self.env['product.supplierinfo'].search([()])        
    #     if recs:
    #         recs_sorted = recs.sorted(key=lambda r: r.price)
    #         return recs_sorted[0].id        
        
    # start_time = fields.Many2one('test.line',string="Start Time", default=_get_min_test)


    # @api.multi
    # def write(self,vals):
    #     if self.shipping_cost:    
    #         vals.update({'standard_price':self.standard_price_from_vendor})                   
    #     return super(SourceProduct, self).write(vals)
       
    dropship_zipcode=fields.Char(string="Shipping Zip",track_visibility="onchange")   
    shipping_country =fields.Char(string="Shipping Country")      
    shipping_state =fields.Char(string="Shipping City")      
    product_url1=fields.Char(string="Product Image Url 1")      
    product_url2=fields.Char(string="Product Image Url 2") 
    product_desciption=fields.Html(string="Product Description")
    product_customer_type=fields.Char(string="Customer Type")

    is_source_product = fields.Boolean(string="Source Product")
    pricing_date = fields.Date(string='Pricing Date')   
    pricing_state=fields.Selection([('draft', 'Pending'),
							('done', 'Pricing Done'),
                            ('cancel','Cancelled'),
                            ('approved','Approve')],'Pricing State', track_visibility="onchange", copy=False, default='draft')
    assigned_to =fields.Many2one('res.users',string="Pricing by")
    priority_task = fields.Selection(AVAILABLE_PRIORITIES, "Priority", default='1')   
    ref_doc_id = fields.Reference(selection='_referencable_models',string='Reference Document')
    shipping_cost =fields.Float(string="Shipping Cost Charged by Vendor",track_visibility="onchange")  
    standard_price_from_vendor=fields.Float(string="Minimum Cost from Vendor",track_visibility="onchange")  
    selected_vendor_from_vendorlist = fields.Char(string='Accepted Vendor',track_visibility="onchange")


    # @api.model
    # def _get_providers(self):
    #     providers = super(AcquirerSips, self)._get_providers()
    #     providers.append(['sips', 'Sips'])
    #     return providers
    #partners.sorted(key=lambda p: (p.name, p.country_id.id))

    @api.multi 
    @api.onchange('seller_ids')
    def _get_selected_vendor(self):
        for obj in self.seller_ids:                   
            if obj.status == 'accepted':             
                values = dict(standard_price_from_vendor=obj.price,
                            shipping_cost=obj.dropshipping_charge,
                            selected_vendor_from_vendorlist=obj.name.name,
                            standard_price=obj.price + obj.dropshipping_charge   
                            )
                self.update(values)
                break                                                           
            elif obj.status == 'underprocess':
                values = dict(standard_price_from_vendor=00.00,
                            shipping_cost=00.00,
                            standard_price=00.00                            
                            )
                self.update(values)             
            elif obj.status == 'rejected':
                values = dict(standard_price_from_vendor=00.00,
                            shipping_cost=00.00,
                            standard_price=00.00                            
                            )
                self.update(values)       
            elif obj.status == 'blacklist':
                values = dict(standard_price_from_vendor=00.00,
                            shipping_cost=00.00,
                            standard_price=00.00                            
                            )
                self.update(values)

    # @api.one
    # @api.depends('seller_ids','standard_price_from_vendor','shipping_cost')
    # def compute_status(self):
    #     for obj in self.seller_ids: 
    #         obj_status=self.env['product.supplierinfo'].search([('status','=','accepted'),('price','=',min('price'))])     
    #         if obj_status:             
    #             values = dict(standard_price_from_vendor=obj.price,
    #                         shipping_cost=obj.dropshipping_charge,
    #                         selected_vendor_from_vendorlist=obj.name.name,
    #                         standard_price=obj.price + obj.dropshipping_charge   
    #                         )
    #             self.update(values)

    # @api.multi 
    # @api.onchange('seller_ids')
    # def _get_selected_vendor(self):
    #     for obj in self.seller_ids: 
    #         obj_status=self.env['product.supplierinfo'].search([('status','=','accepted'),('price','=',[min('price')])])     
    #         if obj_status:             
    #             values = dict(standard_price_from_vendor=obj.price,
    #                         shipping_cost=obj.dropshipping_charge,
    #                         selected_vendor_from_vendorlist=obj.name.name,
    #                         standard_price=obj.price + obj.dropshipping_charge   
    #                         )
    #             self.update(values)
                                                       

    @api.multi 
    @api.depends('pricing_state','standard_price')    
    def btn_SourcingDone(self):
        if self.seller_ids:
            if self.standard_price > 0.00:
                if self.standard_price == float(self.standard_price_from_vendor + self.shipping_cost):
                    self.pricing_date = datetime.now().strftime("%Y-%m-%d")  
                    # self.is_source_product = False
                else:
                    raise UserError(_("Cost Price is Incorrect..Please Check"))    
            else:
                raise UserError(_("Please Check Cost Price Value"))                
        else:
            raise UserError(_("No Vendor For this Product"))            
        return self.write({'pricing_state': 'done'})   

    @api.multi 
    def btn_SourcingCancel(self):
        return self.write({'pricing_state': 'cancel'})            

    @api.multi 
    def btn_SourcingResetoNew(self):
        return self.write({'pricing_state': 'draft'})        

    @api.multi 
    @api.depends('pricing_state','standard_price')    
    def btn_SourcingApprove(self):
        if self.seller_ids:
            if self.standard_price > 0.00:
                if self.standard_price == float(self.standard_price_from_vendor + self.shipping_cost):
                    self.pricing_date = datetime.now().strftime("%Y-%m-%d")  
                    self.is_source_product = False
                else:
                    raise UserError(_("Cost Price is Incorrect..Please Check"))    
            else:
                raise UserError(_("Please Check Cost Price Value"))                
        else:
            raise UserError(_("No Vendor For this Product"))            
        return self.write({'pricing_state': 'approved'}) 