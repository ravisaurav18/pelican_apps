# -*- coding: utf-8 -*-

{
    'name': 'Common Product Pricing',
    'summary': '''Gives Pricing team the access of products ''',
    'images': [],
    'author': 'RSNT',
    'website': '',
    'category': 'Purchase Management',
    'version': '9.0.1.1',
    'license': '',
    'depends': ['product','purchase','sale',
         'mail','fetchmail',
    ],
    'data': [
        'security/ir_rule.xml',
        'security/ir.model.access.csv',                      
        'views/source_product.xml',
        'views/supplierinfo.xml',
        'views/res_partner.xml',
        'views/sale_view.xml',

    ],

    'images': ['static/description/icon.png'],    
    
}
