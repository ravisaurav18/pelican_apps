from openerp.osv import osv,fields

class account_voucher(osv.osv):
	_inherit = "account.voucher"

	def onchange_reference(self, cr, uid, ids, reference, context={}):
		res= {}
		value = {}
		warning = {}
		if reference:
			voucher_ids = self.pool.get('account.voucher').search(cr,uid,[('reference','=',reference)])
			if voucher_ids:
				value = {'reference':False}
				warning = {'title':'Duplicate Reference', 'message':'Payment reference that you enter is already used in any other payment. Please use another reference.'}
		res['value'] = value
		res['warning'] = warning
		return res