{
		"name"          : "Customized Accounting & Finance Module",
		"version"       : "1.0",
		"depends"       : ["base","account","account_voucher"],
		"author"        : "RSNT",
		"description"   : """
This module has customized several aspects in Account & Finance module, including:

- Changing invoice template
""",
		"website"       : "https://www.odesk.com/users/~014ecb73724f396338",
		"category"      : "Accounting & Finance",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							'account_voucher_view.xml',
							# 'views/report_invoice.xml',
							'report/report_invoice.xml',
							'edi/invoice_action_data.xml',
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}