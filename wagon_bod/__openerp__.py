{
		"name"          : "Company Summary for Business Partner",
		"version"       : "1.0",
		"depends"       : ["base","sale","purchase","account"],
		"author"        : "Togar Hutabarat",
		"description"   : """
This module create some menus for top management purpose, such as:

- Quotation and Sales Order
- Purchase Order
- Invoices
""",
		"website"       : "https://www.odesk.com/users/~014ecb73724f396338",
		"category"      : "Tools",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"security/bod_security.xml",
							"bod_view.xml",
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}