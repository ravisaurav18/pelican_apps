<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		.break { page-break-after: always; }
		
		body
		{
			font-size: 12px;
			min-height: 100%;
		}

		table.no_border
		{
		border-collapse:collapse;
		padding:10px;
		width:100%;
		vertical-align: top;
		}
		
		table.grey
		{
		border-collapse:collapse;
		border: 1px solid LightGrey;
		padding:10px;
		width:100%;
		}

		td.grey
		{
		border: 1px solid LightGrey;
		text-align: center;
		}

		table.content
		{
		border-collapse:collapse;
		border: 1px solid black;
		}

		td.content
		{
		border-bottom: 1px solid LightGrey;
		border-left: 1px solid black;
		border-right: 1px solid black;
		vertical-align: top;
		}

		td.total
		{
		border-bottom: 1px solid black;
		vertical-align: top;
		font-weight: bold;
		}

		th.content
		{
		border-collapse: collapse;
		border-bottom: 1px solid black;
		border-left: 1px solid black;
		border-right: 1px solid black;
		}

		.footer 
		{
			position: absolute;
			right: 0;
			bottom: 0;
			left: 0;
			padding: 1rem;
			background-color: #efefef;
			text-align: center;
		}
		td.data
		{
		border-bottom:thin solid black;
		width:69%;
		}
		
		.var
		{
		font-weight:bold;
		}
		
		.left
		{
			text-align: left;
		}

		.center
		{
			text-align: center;
		}

		.right
		{
			text-align: right;
		}

		.top
		{
			vertical-align: top;
		}

		.padded
		{
		text-align:center;
		padding-top:30px;
		}
		
		.ttd
		{
		text-align:center;
		padding-bottom:80px;
		width:50%
		}
		
		.nama
		{
		text-align:center;
		width:50%;
		font-weight:bold;
		}
		
		
	</style>
</head>
%for o in objects:
<body>
	<h2 class="center">PACKING LIST</h2>

	<hr/>

	<table width="100%" cellpadding="10px">
		<tr>
			<td width="5%" class="top">
				<b>From:</b>
			</td>
			<td width="45%" class="top">
				${o.company_id.name} <br/>
				${o.company_id.street}, ${o.company_id.city} <br/>
				${o.company_id.state_id.name}, ${o.company_id.state_id.code}, ${o.company_id.zip}
			</td>
			<td width="5%" class="top">
				<b>To:</b>
			</td>
			<td width="45%" class="top">
				<b>${o.dest_address_id.name or ''}</b> <br/>
				${o.dest_address_id.street or ''}, ${o.dest_address_id.street2 or ''} <br/>
				${o.dest_address_id.city or ''}
				${o.dest_address_id.state_id and o.dest_address_id.state_id.name or ''}, ${o.dest_address_id.state_id and o.dest_address_id.state_id.code or ''}, ${o.dest_address_id.zip or ''}
			</td>
		</tr>
	</table>
 	
 	<p/>
	<table width="100%" class="content" cellpadding="2px">
		<tr>
			<td width="15%"> <b>Ship Method</b> </td>
			<td width="35%">: ${o.dropship_shipping_method or ''}</td>

			<td width="15%"> <b>Order Number</b> </td>
			<td width="35%">: ${o.origin or ''}</td>
		</tr>
		<tr>
			<td width="15%"> <b>Ref</b> </td>
			<td width="35%">: ${o.name or ''}</td>

			<td width="15%"> <b>Order Date</b> </td>
			<td width="35%">: ${o.date_order and time.strftime('%b %d, %y', time.strptime( o.date_order,'%Y-%m-%d %H:%M:%S')) or ''}</td>
		</tr>
	</table>

	<br/><br/>

	<table width="100%" cellpadding="0px" class="content">
		<tr>
			<th width="5%" class="content" style="padding: 5px">Number</th>
			<th width="15%" class="content" style="padding: 5px">Item Code</th>
			<th width="70%" class="content" style="padding: 5px">Description</th>
			<th width="15%" class="content" style="padding: 5px">Qty</th>
		</tr>
		<% set i=1 %>
		%for line in o.order_line:
		<tr>
			<td class="content top center" style="padding: 2px">
				${i}
			</td>
			<td class="content top left" style="padding: 2px">
				${line.product_id.default_code or ''}
			</td>
			<td class="content top left" style="padding: 2px">
				${line.product_id.name}
			</td>
			<td class="content top center" style="padding: 2px">
				${line.product_qty}
			</td>
		</tr>
		<% set i=i+1 %>
		%endfor
		<tr>
			<td colspan="4" class="data"></td>
		</tr>
	</table>
	<p/>
</body>
%endfor
</html>