from openerp.report import report_sxw
from openerp import api, models
from openerp.osv import osv,fields

class pelican_purchase_report_parser(report_sxw.rml_parse):
	
	def __init__(self, cr, uid, name, context):
		super(pelican_purchase_report_parser, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
			'vendor_part_number': self._get_vendor_part_number,
		})
	
	def _get_vendor_part_number(self, product, vendor):
		part_number = product.default_code
		for supinfo in product.seller_ids:
			print supinfo.name, vendor
			if supinfo.name.id == vendor.id:
				part_number = supinfo.product_code
		return part_number

class wrapped_report_purchasequotation(osv.AbstractModel):
	_name = 'report.purchase.report_purchasequotation'
	_inherit = 'report.abstract_report'
	_template = 'purchase.report_purchasequotation'
	_wrapped_report_class = pelican_purchase_report_parser

class wrapped_report_purchaseorder(osv.AbstractModel):
	_name = 'report.purchase.report_purchaseorder'
	_inherit = 'report.abstract_report'
	_template = 'purchase.report_purchaseorder'
	_wrapped_report_class = pelican_purchase_report_parser



