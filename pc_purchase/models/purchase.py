from openerp.osv import osv,fields
from openerp.tools.translate import _

class purchase_order(osv.osv):
	_inherit = "purchase.order"

	def _get_picking_progress(self,cr,uid,ids,field,args, context=None):
		value = {}
		for data in self.browse(cr,uid,ids):
			total = 0.0
			total_all = 0.0
			for line in data.picking_ids:
				# for d_line in [d_lines for d_lines in line.move_lines if d_lines.purchase_id.id == data.id]:
				for d_line in line.move_lines:
					total += (d_line.state == 'done' and d_line.product_qty or 0.0)
			for line2 in data.order_line:
				total_all += line2.product_qty
			value[data.id] = (total_all!=0.0) and (total / total_all * 100) or 0.0
		return value

	def _get_invoicing_progress(self,cr,uid,ids,field,args, context=None):
		value = {}
		for data in self.browse(cr,uid,ids):
			total_paid = 0.0
			total_all = 0.0
			for line in data.invoice_ids:
				print "1",line
				if line.state in ('draft','cancel'):continue
				total_paid += line.amount_total-line.residual
				total_all += line.amount_total
			value[data.id] = ((total_all>0.0) and ((total_paid/total_all)*100)) or 0.0
		return value

	def _get_order_by_invoice(self, cr, uid, ids, context=None):
		result = {}
		for invoice in self.pool.get('account.invoice').browse(cr, uid, ids, context=context):
			order_ids = self.pool.get('purchase.order').search(cr,uid,[('name','=',invoice.origin)])
		return order_ids

	def _get_purchase_order(self, cr, uid, ids, context=None):
		result = {}
		for order in self.browse(cr, uid, ids, context=context):
			result[order.id] = True
		return result.keys()

	_columns = {

		'progress_picking': fields.function(_get_picking_progress,string="Shipment Progress",type="float",store = True),
		'progress_invoicing': fields.function(_get_invoicing_progress,string="Invoice Progress",type="float",
			store = {
				'account.invoice': (_get_order_by_invoice, ['state'], 10),
				'purchase.order': (_get_purchase_order, ['invoice_ids'], 10),
			}),
		# 'dropship_shipping_mode':fields.many2one('delivery.carrier','Delivery Carrier',track_visibility="onchange"),
	}




	def wkf_send_rfq_without_attachment(self, cr, uid, ids, context=None):
		'''
		This function opens a window to compose an email, with the edi purchase template message loaded by default
		'''
		if not context:
			context= {}
		ir_model_data = self.pool.get('ir.model.data')
		try:
			if context.get('send_rfq', False):
				template_id = ir_model_data.get_object_reference(cr, uid, 'ow_purchase', 'email_template_edi_without_attachment_purchase')[1]
			else:
				template_id = ir_model_data.get_object_reference(cr, uid, 'ow_purchase', 'email_template_edi_without_attachment_purchase_done')[1]
		except ValueError:
			template_id = False
		try:
			compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
		except ValueError:
			compose_form_id = False 
		ctx = dict(context)
		ctx.update({
			'default_model': 'purchase.order',
			'default_res_id': ids[0],
			'default_use_template': bool(template_id),
			'default_template_id': template_id,
			'default_composition_mode': 'comment',
		})
		return {
			'name': _('Compose Email'),
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'mail.compose.message',
			'views': [(compose_form_id, 'form')],
			'view_id': compose_form_id,
			'target': 'new',
			'context': ctx,
		}

	def wkf_send_rfq_without_attachment1(self, cr, uid, ids, context=None):
		'''
		This function opens a window to compose an email, with the edi purchase template message loaded by default
		'''
		if not context:
			context= {}
		ir_model_data = self.pool.get('ir.model.data')
		try:
			if context.get('send_rfq', False):
				template_id = ir_model_data.get_object_reference(cr, uid, 'ow_purchase', 'email_template_edi_without_attachment_purchase')[1]
			else:
				template_id = ir_model_data.get_object_reference(cr, uid, 'ow_purchase', 'email_template_edi_without_attachment_purchase_done')[1]
		except ValueError:
			template_id = False
		try:
			compose_form_id = ir_model_data.get_object_reference(cr, uid, 'mail', 'email_compose_message_wizard_form')[1]
		except ValueError:
			compose_form_id = False 
		ctx = dict(context)
		ctx.update({
			'default_model': 'purchase.order',
			'default_res_id': ids[0],
			'default_use_template': bool(template_id),
			'default_template_id': template_id,
			'default_composition_mode': 'comment',
		})
		return {
			'name': _('Compose Email'),
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'mail.compose.message',
			'views': [(compose_form_id, 'form')],
			'view_id': compose_form_id,
			'target': 'new',
			'context': ctx,
		}