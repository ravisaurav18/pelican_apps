{
		"name"          : "Base Module for Office Wagon",
		"version"       : "1.0",
		"depends"       : ["base"],
		"author"        : "Togar Hutabarat",
		"description"   : """
This module has customized several aspects in base module, including:

- New header for all new template for Office Wagon
""",
		"website"       : "https://www.odesk.com/users/~014ecb73724f396338",
		"category"      : "Base",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"data.xml",
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}