{
		"name"          : "OW Customized Purchase Management",
		"version"       : "1.0",
		"depends"       : ["base","base_setup","purchase","wagon_sale","report_webkit","report"],
		"author"        : "RSNT",
		"description"   : """
This module has customized several aspects in Purchasing Management module, including:

- RFQ / Purchase Order template
""",
		"website"       : "",
		"category"      : "Purchases",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							'views/purchase_view.xml',
							'data/purchase_order_action_data.xml',
							#'views/report_purchase.xml',
							'report/report_purchase_order.xml'
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}