# -*- coding: utf-8 -*-

{
    'name': 'PC Container Delivery Method',
    'summary': ''' ''',
    'images': [],
    'author': 'RSNT',
    'website': '',
    'category': 'Purchase Management',
    'version': '9.0.1.1',
    'license': '',
    'depends': ['purchase','delivery',
             ],
    'data': [
        'views/purchase_view.xml', 
  
    ],

    'images': ['static/description/icon.png'],    
    
}
