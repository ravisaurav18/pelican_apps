from openerp import models,fields,api,_
from openerp.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)


Container_Delivery_to_Customer_mode = [
    ('1', 'Customer Pick Up'),
    ('2', 'Vendor Dropship'),
    ('3', 'Hire a Delivery Carrier'),
]

class PurchaseDelivery(models.Model):
    _inherit = 'purchase.order'    
    
    customer_address=fields.Text('Customer Address')
    container_delivery_method=fields.Selection(Container_Delivery_to_Customer_mode, "Delivery to Customer", default='1')     
    dropship_shipping_mode=fields.Many2one('delivery.carrier','Delivery Carrier',track_visibility="onchange",ondelete="cascade")


	# @api.multi
	# def get_shipping_address(self):
	    #customer_address=fields.Char(related='origin.partner_shipping_id.city',string='Customer City')		  