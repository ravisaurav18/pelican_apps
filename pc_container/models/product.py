#from openerp.osv import osv,fields
from openerp import models,fields,api,_
from openerp.exceptions import UserError
from openerp.exceptions import ValidationError
from difflib import SequenceMatcher
from openerp.exceptions import except_orm, Warning
from openerp import SUPERUSER_ID
import re

class PcProductContainerInfoAdded(models.Model):
	_description = "Product Container Information"
	_inherit = "product.template"



	product_container_location_type=fields.Many2one("container.location.type",string="Container Location Type")  
	product_container_types=fields.Many2one("container.type",string="Container Type") 	
	product_container_location=fields.Many2one("container.location",string='Container Location')
	is_container =fields.Boolean(string="Is Container")
	container_categ_id=fields.Many2one('product.category',string='Product Category', domain="[('type','=','normal')]" ,help="Select category for the current product")


	_sql_constraints = [('name_uniq', 'unique(name)', 'Entered Product name Exist in System....'),
            ] 

	@api.multi
	@api.onchange('container_categ_id')		
	def name_get11(self):
		for record in self:
			if record.is_container == True:
				record.name=''
				internal = str(record.container_code)
				val = internal + " -- "+record.container_categ_id.name
				values = dict(name=val)	
				record.update(values)

	def create(self,cr,uid,vals,context=None):
		all_product = self.search(cr,SUPERUSER_ID,[('is_container','=',True),('active','=',True)])
		for product in self.browse(cr,SUPERUSER_ID,all_product):
			if vals.get('name',False):
				similarity = SequenceMatcher(None, vals['name'].upper().strip(), product.name.upper().strip()).ratio()
				if similarity >= 0.95:
					raise except_orm(_('Product Already Exist!'), _('There is one (or more) product that'\
												' has similar name.\n Please contact your Product Manager'))
		return super(PcProductContainerInfoAdded,self).create(cr,uid,vals,context=None)


	# @api.model
	# def create(self, vals):
	# 	# if vals.get('code', '/') == '/':
	# 	vals['default_code'] = self.env['ir.sequence'].next_by_code('container.container')
	# 	return super(OwProductContainerInfoAdded, self).create(vals)