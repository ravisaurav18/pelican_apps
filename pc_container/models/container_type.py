#from openerp.osv import osv,fields
from openerp import models,fields,api,_
from openerp.exceptions import UserError
from openerp.exceptions import ValidationError
import re

class PcContainerType(models.Model):
	_description = "Container Location"
	_name = "container.type"

	name=fields.Char(string="Container Type")  

