#from openerp.osv import osv,fields
from openerp import models,fields,api,_
from openerp.exceptions import UserError
from openerp.exceptions import ValidationError
import re

CONTAINER_LOCATION_TYPE = [
    ('container_yard', 'Container Yard'),
    ('ocean_port', 'Ocean Port'),
    ('inland_port', 'Inland Port'),
    ('business', 'Business'),
    ('contraction', 'Contraction'),   
    ('trade_show', 'Trade Show')     
]

class PcContainerLocationType(models.Model):
	_description = "Container Location Type"
	_name = "container.location.type"

	name=fields.Char(string="Container Location Type")      

