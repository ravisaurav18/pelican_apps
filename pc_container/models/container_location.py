#from openerp.osv import osv,fields
from openerp import models,fields,api,_
from openerp.exceptions import UserError
from openerp.exceptions import ValidationError
import re



class PcContainerLocation(models.Model):
	_description = "Container Location"
	_name = "container.location"

	name=fields.Char(string="Container Location")  
	state_id=fields.Many2one("res.country.state",string='State')
	country_id=fields.Many2one('res.country', string='Country')

	@api.multi
	def onchange_state(self, state_id):
		if state_id:
			state = self.env['res.country.state'].browse(state_id)
			return {'value': {'country_id': state.country_id.id}}
		return {'value': {}}	