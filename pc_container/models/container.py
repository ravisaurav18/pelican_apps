#from openerp.osv import osv,fields
from openerp import models,fields,api,_
from openerp.exceptions import UserError
from openerp.exceptions import ValidationError
import re

# class OwContainer(models.Model):
# 	_description = "Container"
# 	_name = "container.container"

# 	name=fields.Char(string="Container No") 
# 	demo=fields.Char(string="Demo")
# 	sale_line_ids = fields.Many2many('sale.order.line', 'sale_order_line_container_rel', 'container_id', 'order_line_id', string='Sale Order Lines', readonly=True, copy=False)


# class ContainerSaleOrderLine(models.Model):
# 	_inherit = 'sale.order.line'

# 	container_lines = fields.Many2many('container.container', 'sale_order_line_container_rel', 'order_line_id', 'container_id', string='Container Lines', copy=False)




class ContainerSaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	container_lines = fields.One2many('container.container','sales_line_id',string='Container Lines')


class PcContainer(models.Model):
	_description = "Container"
	_name = "container.container"

	name=fields.Char(string="Container No",required=True) 
	sales_line_id = fields.Many2one('sale.order.line', string='Sales Order Lines',ondelete="restrict")
	order_id = fields.Many2one('sale.order',string='Sales Order')
	date=fields.Datetime('Created date', default=fields.Datetime.now)

	@api.multi
	@api.onchange('sales_line_id')
	def onchange_order_line_id(self):
		self.order_id = self.sales_line_id.order_id