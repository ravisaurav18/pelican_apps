#from openerp.osv import osv,fields
from openerp import models,fields,api,_
from openerp.exceptions import UserError
from openerp.exceptions import ValidationError
import re

class PcProductContainerPurchase(models.Model):
	_description = "Container Purchase Information"
	_inherit = "purchase.order"

	release_detail =fields.Char(string="Release Detail")