{
		"name"          : "Pc Container",
		"version"       : "1.0",
		"depends"       : ["base",'product','sale','purchase','cm_product_pricing',],
		"author"        : "RSNT",
		"description"   : """
						  This module will add Extra Tab to Product module.
						  """,
		"website"       : "",
		"category"      : "Product",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"views/product_view.xml",
							#"sequence/sequence.xml",
							"views/container_location_type.xml",
							"views/container_location.xml",							
							"views/container_type.xml",
							"views/container_view.xml",							
							"data/data_container_location_type.xml",
							"data/data_container_location.xml",	
							"security/ir.model.access.csv",
							"views/purchase_views.xml",						
							],
		"active"        : True,
		"application"   : True,
		"installable"   : True,
}