{
		"name"          : "Customized Mail Module",
		"version"       : "1.0",
		"depends"       : ["base","mail"],
		"author"        : "Togar Hutabarat",
		"description"   : """
This module will add functionality to configure multiple outgoing mail server for different purpose.
""",
		"website"       : "https://www.odesk.com/users/~014ecb73724f396338",
		"category"      : "Sales",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"mail_view.xml",
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}