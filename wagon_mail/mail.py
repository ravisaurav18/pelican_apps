from openerp.osv import osv,fields
from openerp import SUPERUSER_ID, api

class ir_mail_server(osv.osv):
	_inherit = "ir.mail_server"
	_columns = {
		'owner_user_id': fields.many2one('res.users','User')
	}

class mail_compose_message(osv.TransientModel):
	_inherit = 'mail.compose.message'
	def send_mail(self, cr, uid, ids, context=None):
		""" Process the wizard content and proceed with sending the related
			email(s), rendering any template patterns on the fly if needed. """
		context = dict(context or {})

		# clean the context (hint: mass mailing sets some default values that
		# could be wrongly interpreted by mail_mail)
		context.pop('default_email_to', None)
		context.pop('default_partner_ids', None)

		server = self.pool.get('ir.mail_server').search(cr,uid,[('owner_user_id','=',uid)])
		self.write(cr,uid,ids,{'mail_server_id':server and server[0] or False})
		for wizard in self.browse(cr, uid, ids, context=context):
			mass_mode = wizard.composition_mode in ('mass_mail', 'mass_post')
			active_model_pool = self.pool[wizard.model if wizard.model else 'mail.thread']
			if not hasattr(active_model_pool, 'message_post'):
				context['thread_model'] = wizard.model
				active_model_pool = self.pool['mail.thread']

			# wizard works in batch mode: [res_id] or active_ids or active_domain
			if mass_mode and wizard.use_active_domain and wizard.model:
				res_ids = self.pool[wizard.model].search(cr, uid, eval(wizard.active_domain), context=context)
			elif mass_mode and wizard.model and context.get('active_ids'):
				res_ids = context['active_ids']
			else:
				res_ids = [wizard.res_id]

			batch_size = int(self.pool['ir.config_parameter'].get_param(cr, SUPERUSER_ID, 'mail.batch_size')) or self._batch_size

			sliced_res_ids = [res_ids[i:i + batch_size] for i in range(0, len(res_ids), batch_size)]
			for res_ids in sliced_res_ids:
				all_mail_values = self.get_mail_values(cr, uid, wizard, res_ids, context=context)
				for res_id, mail_values in all_mail_values.iteritems():
					if wizard.composition_mode == 'mass_mail':
						self.pool['mail.mail'].create(cr, uid, mail_values, context=context)
					else:
						subtype = 'mail.mt_comment'
						if wizard.is_log or (wizard.composition_mode == 'mass_post' and not wizard.notify):  # log a note: subtype is False
							subtype = False
						if wizard.composition_mode == 'mass_post':
							context = dict(context,
										   mail_notify_force_send=False,  # do not send emails directly but use the queue instead
										   mail_create_nosubscribe=True)  # add context key to avoid subscribing the author
						active_model_pool.message_post(cr, uid, [res_id], type='comment', subtype=subtype, context=context, **mail_values)

		return {'type': 'ir.actions.act_window_close'}