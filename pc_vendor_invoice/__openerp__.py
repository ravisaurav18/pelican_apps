{
		"name"          : "PC Customized Vendor Invoice Report",
		"version"       : "1.0",
		"depends"       : ["base","account","account_voucher"],
		"author"        : "RSNT",
		"description"   : """
This module has customized several aspects in Account & Finance module, including:

- Changing invoice template
""",
		"website"       : "",
		"category"      : "Accounting & Finance",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							'edi/data_invoice.xml',										
							'report/report_pc_invoice.xml',
							'edi/invoice_action_data.xml',
							
							],
		"active"        : False,
		"application"   : True,
		"installable"   : True,
}