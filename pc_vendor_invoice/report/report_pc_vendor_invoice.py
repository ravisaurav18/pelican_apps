from openerp.addons.report_webkit import webkit_report
from openerp.addons.report_webkit.report_helper import WebKitHelper
from openerp.addons.report_webkit.webkit_report import webkit_report_extender
from openerp.report import report_sxw
from datetime import date
from datetime import datetime
import time
import unicodedata

class report_template_invoice_pc(report_sxw.rml_parse):
	
	def __init__(self, cr, uid, name, context):
		super(report_template_invoice_pc, self).__init__(cr, uid, name, context=context)
		self.localcontext.update({
								  'get_object'      : self._get_object ,
								  # 'get_taxes'		: self._get_taxes ,
								  })

	def _get_object(self,data):
		obj_data=self.pool.get(data['model']).browse(self.cr,self.uid,[data['id']])
		return obj_data

	# def _get_taxes(self,tax_line_ids,code):
	# 	tax_amount=0.0
	# 	for tax in tax_line_ids:
	# 		split_tax_description = tax.name.split(' ')
	# 		if code in split_tax_description:
	# 			tax_amount = tax.amount
	# 		else:
	# 			return False
	# 	return tax_amount

webkit_report.WebKitParser('report.account.report_invoice.pc', 'account.invoice', 'pc_vendor_invoice/report/report_pc_vendor_invoice.mako', parser=report_template_invoice_pc)
