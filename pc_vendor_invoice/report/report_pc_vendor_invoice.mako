<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		.break { page-break-after: always; }
		
		body
		{
			font-size: 12px;
			min-height: 100%;
		}

		table.no_border
		{
			border-collapse:collapse;
			padding:10px;
			width:100%;
			vertical-align: top;
		}
		
		table.grey
		{
			border-collapse:collapse;
		/*	border: 1px solid LightGrey;
*/			border: 1px solid #BE90D4;			
			padding:10px;
			width:100%;
		}

		td.grey
		{
			border: 1px solid #BE90D4;
			text-align: center;
		}

		table.content
		{
			border-collapse:collapse;
			border: 1px solid black;
		}

		td.content
		{
			border-bottom: 1px solid #BE90D4;
			vertical-align: top;
		}

		.hright
		{
			text-align: right;
		}

		.hmid
		{
			text-align: center;
		}

		td.total
		{
			border-bottom: 1px solid black;
			vertical-align: top;
			font-weight: bold;
		}

		th.content
		{
			border-collapse: collapse;
			border-bottom: 1px solid black;
		}

		.footer 
		{
			position: absolute;
			right: 0;
			bottom: 0;
			left: 0;
			padding: 1rem;
			background-color: #efefef;
			text-align: center;
		}
		td.data
		{
			border-bottom:thin solid black;
			width:69%;
		}
		
		.var
		{
			font-weight:bold;
		}
		
		.left
		{
			text-align: left;
		}

		.center
		{
			text-align: center;
		}

		.right
		{
			text-align: right;
		}

		.padded
		{
			text-align:center;
			padding-top:30px;
		}
		
		.ttd
		{
			text-align:center;
			padding-bottom:80px;
			width:50%
		}
		
		.nama
		{
			text-align:center;
			width:50%;
			font-weight:bold;
		}
		pre
		{
			display: inline;
			margin: 0;
		}
		#watermark {
		  color: #BE90D4;
		  text-align:center;
		  font-size: 80pt;
		  -webkit-transform: rotate(-45deg);
		  position: fixed;
		  width: 500px;
		  height: 500px;
		  margin: 0;
		  z-index: -1;
		  left:230px;
		  top:300px;
			}
		
	</style>
</head>
<body>
	%for o in objects:
		<table width="100%">
			<tr>
				<td style="font-size: 15px; vertical-align: middle;">
					Business Name: ${objects.company_id.name or ''} <br />
					${objects.company_id.street or ''} <br />
					%if o.company_id.street2:
						${o.company_id.street2 or ''}
					%endif					
					${objects.company_id.city or ''}, ${objects.company_id.state_id and objects.company_id.state_id.code or ''} - ${objects.company_id.zip or ''}<br/>
					${o.company_id.country_id and o.company_id.country_id.code or ''}<br />
					Phone: ${objects.company_id.phone or '-'} | Fax: ${objects.company_id.fax or '-'} <br />
				</td>
				<td style="text-align: right; vertical-align: middle;font-size: 12px;">
					%if o.type in ['out_invoice'] and o.state in ['open','paid']:
						<b>Invoice Number:</b> ${o.number or ''}<br/>
					    <b>Invoice Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />
					%elif o.type in ['in_invoice'] and o.state in ['open','paid']:
						<b>Bill Number:</b> ${o.number or ''}<br/>
					    <b>Bill Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />						
					%elif o.type in ['out_invoice'] and o.state == 'draft':
					    <b>Draft Invoice:</b> ${o.number or ''}<br/>
					    <b>Invoice Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />

					%elif o.type in ['in_invoice'] and o.state == 'draft':
					    <b>Draft Bill:</b> ${o.number or ''}<br/>
					    <b>Bill Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />					    
					%elif o.type in ['out_invoice'] and o.state == 'cancel':
						<b>Canceled Invoice:</b> ${o.number or ''}<br/>
					    <b>Canceled Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />

					%elif o.type in ['in_invoice'] and o.state == 'cancel':
						<b>Canceled Bill:</b> ${o.number or ''}<br/>
					    <b>Canceled Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />						
					%elif o.type == 'out_refund':
						<b>Customer Refund:</b> ${o.number or ''}<br/>
					    <b>Refund Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />
						
					%elif o.type == 'in_refund':
						<b>Vendor Refund:</b> ${o.number or ''}<br/>
					    <b>Refund Date:</b>${o.date_invoice and time.strftime('%m/%d/%Y',time.strptime( o.date_invoice,'%Y-%m-%d')) or 'N/A'}<br />						
					%endif	
							
				</td>
				<br/>
			</tr>

			<tr>
				<br/><br/>
				<td width="40%" style="vertical-align: top;">
					<table class="grey" width="100%" cellpadding="3px">
						<tr>
							<td class="grey" width="40%" style="background-color: #BE90D4; color: white; font-weight: bold;">Bill To</td>
							<td class="grey" width="60%" colspan="2">${o.partner_id.name or ''}</td>
						</tr>
						<tr>
							<td class="grey" colspan="2" style="text-align: left; vertical-align: middle;font-size: 12px;">
								${o.partner_id.name or ''} <br/>
								${o.partner_id.street or ''},
								%if o.partner_id.street2:
									${o.partner_id.street2 or ''}
								%endif								
								${o.partner_id.city or ''}, <br/>
								%if o.partner_id.state_id:
									${o.partner_id.state_id and o.partner_id.state_id.code or ''},
								%endif
								${o.partner_id.zip or ''} 
								${o.partner_id.country_id and o.partner_id.country_id.code or ''} <br />
								Phone: ${o.partner_id.phone or ''}<br />
								%if o.partner_id.email:
									Email: ${o.partner_id.email or ''}<br />
								%endif								
								%if o.partner_id.fax:
									Fax: ${o.partner_id.fax or ''}<br />
								%endif								

							</td>
						</tr>
					</table>
				</td>
				<td width="40%" style="vertical-align: top;">				
					<table class="grey" width="100%" cellpadding="3px">
						<tr>
							<td class="grey" width="40%" style="background-color: #BE90D4; color: white; font-weight: bold;">Deliver/PickUP</td>
							<td class="grey" width="60%" colspan="2">${''}</td>
						</tr>
						<tr>
							<td class="grey" colspan="2" style="text-align: left; vertical-align: middle;font-size: 12px;">
								${''} <br/>
								${''} <br/>
								${''} <br/>											
								${''} <br/>
								${''} <br/>											

							</td>
						</tr>
					</table>
				</td>				
			</tr>

		</table><br/>

	<table class="grey" cellpadding="8px">
		<tr>
			<td width="11%" class="grey" style="font-size: 11px;">
				<b>Order No:</b>  <br />
				${o.origin or 'N/A'}
			</td>					
			<td width="11%" class="grey" style="font-size: 11px;">
				<b>Release No:</b>  <br />
				
			</td>
			<td width="11%" class="grey" style="font-size: 11px;">
				<b>Due Date:</b>  <br />
				${o.date_due or 'N/A'}
			</td>
			<td width="11%" class="grey" style="font-size: 11px;">
				<b>Invoice Status:</b>  <br />
				${o.state or 'N/A'}
			</td>
		</tr>
	</table>
	<br/>
	<table width="100%" cellspacing="0px" cellpadding="5px">
		<tr>
			<th width="35%" class="content">Description</th>
			<th width="10%" class="content hright">Quantity</th>
			<th width="15%" class="content hright">Unit Price</th>
			<th width="25%" class="content hright">Taxes</th>
			<th width="15%" class="content hright">Amount</th>
		</tr>
		%for line in o.invoice_line_ids:
		<tr>
			<td class="content">${line.name|safe or ''}</td>
			<td class="content hright">${formatLang(line.quantity,digits=0)} ${line.uom_id and line.uom_id.name or ''}</td>
			<td class="content hright">${formatLang(line.price_unit,digits=2)}</td>
			<td class="content hright">
			%for tax in line.invoice_line_tax_ids:
				%if tax.child_ids:
					%for child in tax.child_ids:
						${child.name} <br/>
					%endfor
				%else:
					${tax.name or ''}
				%endif
			%endfor
			</td>
			<td class="content hright">${formatLang(line.price_subtotal,digits=2)} $</td>
		</tr>
		%endfor
		<tr>
			<td colspan="3"></td>
			<td class="total"><b>Subtotal</b></td>
			<td class="total hright">${formatLang(o.amount_untaxed,digits=2)} $</td>
		</tr>



		<tr>
			<td colspan="3"></td>
			<td class="total"><b>Total</b></td>
			<td class="total hright">${formatLang(o.amount_total,digits=2)} $</td>
		</tr>
		<tr class="border-black">
			<td colspan="3"></td>
			<td class="total"><b>Amount Due</b></td>
			<td class="total hright">${formatLang(o.residual,digits=2)} $</td>
		</tr>
		%if o.state == 'paid':
		<div id="watermark">PAID</div>
		%endif
	</table>
	
		<br /><br />
		<br /><br />


		%if o.type == 'out_invoice':
		 <table width="100%">
			<tr>
				<td style="font-size: 13px; vertical-align: middle;">
					PLEASE BE ADVISED:<br />
					* Payment must be received within 48 hours<br />
					* All containers are sold as is where is<br />
					* All containers are to be picked up within 7 days of receiving your release number <br />
				</td>
			</tr>
		</table><br/>
		%endif
		

	<table width="50%" cellspacing="0px" cellpadding="5px">
	%if o.payment_move_line_ids:
		<tr>			
			<td style="font-size: 13px; vertical-align: middle;">
				Payment History:<br />
			</td>
		</tr>		
		<tr>
			<th width="15%" class="content">Date</th>
			<th width="15%" class="content hright">Reference</th>
			<th width="10%" class="content hright">Type</th>
			<th width="10%" class="content hright">Amount</th>
		</tr>
		%for line in o.payment_move_line_ids:
		<tr>
			<td class="content">${line.date or ''}</td>
			<td class="content hright">${line.move_id.name  or ''}</td>
			<td class="content hright">${line.journal_id.type  or ''}</td>
			<td class="content hright">${line.credit or line.debit or ''}</td>		
		</tr>
		%endfor
	%else:
		* No Payment History for this Invoice  
	%endif			
	</table>


	<br/>
	<h3>${o.payment_term_id and o.payment_term_id.name or ''}</h3>

	%endfor

</body>
</html>
