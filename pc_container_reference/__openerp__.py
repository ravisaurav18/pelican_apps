{
		"name"          : "PC Container Reference ",
		"version"       : "1.0",
		"depends"       : ["base",'product','pc_container'],
		"author"        : "RSNT",
		"description"   : """
						  This module will add Sequence No to Containers.
						  """,
		"website"       : "",
		"category"      : "Product",
		"init_xml"      : [],
		"demo_xml"      : [],
		'test'          : [],
		"data"          : [
							"product_view.xml",
							"sequence/sequence.xml",
							],
		"active"        : True,
		"application"   : True,
		"installable"   : True
}