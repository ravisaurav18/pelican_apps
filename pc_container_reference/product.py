#from openerp.osv import osv,fields
from openerp.osv import fields, osv
from datetime import datetime
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.exceptions import UserError


class PcContainerRef(osv.osv):
    _inherit = 'product.template'

    _columns = {
        'container_code': fields.char('Container Reference', required=True, copy=False),    
    }

    _defaults = {
        'container_code': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').next_by_code(cr, uid, 'product.template'),
    }        